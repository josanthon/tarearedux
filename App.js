import React from 'react'
import {View, Text, StyleSheet} from 'react-native'

const App = ()=>{
  return(
    <>
      <Text style={styles.encabezado}>hola mundo</Text>
    </>
  )  
}

const styles = StyleSheet.create({
  encabezado:{
    textAlign:'center',
    marginTop:50
  }
});

export default App;
